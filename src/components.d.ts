/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import '@stencil/core';




export namespace Components {

  interface ImgnUpldr {
    'imageInput': string;
    /**
    * Props
    */
    'imageTypes': string;
    'maxImageNumber': number;
    'maxImageSize': number;
    'maxPostSize': number;
    'multiple': boolean;
  }
  interface ImgnUpldrAttributes extends StencilHTMLAttributes {
    'imageInput'?: string;
    /**
    * Props
    */
    'imageTypes'?: string;
    'maxImageNumber'?: number;
    'maxImageSize'?: number;
    'maxPostSize'?: number;
    'multiple'?: boolean;
  }
}

declare global {
  interface StencilElementInterfaces {
    'ImgnUpldr': Components.ImgnUpldr;
  }

  interface StencilIntrinsicElements {
    'imgn-upldr': Components.ImgnUpldrAttributes;
  }


  interface HTMLImgnUpldrElement extends Components.ImgnUpldr, HTMLStencilElement {}
  var HTMLImgnUpldrElement: {
    prototype: HTMLImgnUpldrElement;
    new (): HTMLImgnUpldrElement;
  };

  interface HTMLElementTagNameMap {
    'imgn-upldr': HTMLImgnUpldrElement
  }

  interface ElementTagNameMap {
    'imgn-upldr': HTMLImgnUpldrElement;
  }


  export namespace JSX {
    export interface Element {}
    export interface IntrinsicElements extends StencilIntrinsicElements {
      [tagName: string]: any;
    }
  }
  export interface HTMLAttributes extends StencilHTMLAttributes {}

}
