import { Component, Prop, State, Watch } from '@stencil/core';

@Component({
  tag: 'imgn-upldr',
  styleUrl: 'imgn-upldr.scss',
  shadow: true,
})
export class IMGNUpldr {
  /**
   * Refs
   */
  imageWrapper!: HTMLDivElement;

  /**
   * Props
   */
  @Prop() imageTypes: string;
  @Prop() imageInput: string;
  @Prop() maxImageNumber: number;
  @Prop() maxImageSize: number;
  @Prop() maxPostSize: number;
  @Prop() multiple: boolean;

  /**
   * State
   */
  @State() imageNumber = 0;
  @State() postSize = 0;
  @State() allowedImageTypes: string[];

  /**
   * Watchers
   */
  @Watch('imageTypes')
  validateImageTypes(value: string) {
    let imageTypes = [];
    const allowedImageTypes = [];
    imageTypes = value.split('|');

    if (value !== undefined) {
      for (const imageType of imageTypes) {
        switch (imageType.toLowerCase()) {
          case 'gif': case 'image/gif':
            if (allowedImageTypes.indexOf('image/gif') === -1) {
              allowedImageTypes.push('image/gif');
            }

            break;
          case 'jpg': case 'jpeg': case 'image/jpg': case 'image/jpeg':
            if (allowedImageTypes.indexOf('image/jpeg') === -1) {
              allowedImageTypes.push('image/jpeg');
            }

            break;
          case 'png': case 'image/png':
            if (allowedImageTypes.indexOf('image/png') === -1) {
              allowedImageTypes.push('image/png');
            }

            break;
          case 'svg': case 'image/svg': case 'svg+xml': case 'image/svg+xml':
            if (allowedImageTypes.indexOf('image/svg+xml') === -1) {
              allowedImageTypes.push('image/svg+xml');
            }

            break;
          case 'x-icon': case 'image/x-icon':
            if (allowedImageTypes.indexOf('image/x-icon') === -1) {
              allowedImageTypes.push('image/x-icon');
            }

            break;
          default:
            break;
        }
      }

      if (allowedImageTypes.length > 0) {
        this.allowedImageTypes = allowedImageTypes;
      } else {
        this.allowedImageTypes = [
          'image/gif',
          'image/jpeg',
          'image/png',
          'image/svg+xml',
          'image/x-icon',
        ];
      }
    }
  }

  /**
   * On Component Loaded
   */
  componentWillLoad() {
    this.validateImageTypes(this.imageTypes);
  }

  /**
   * Helper Methods
   */
  addRemoveImageButton(event: MouseEvent, removeImageButton: HTMLButtonElement) {
    event.stopPropagation();
    removeImageButton.setAttribute('class', 'remove-image-button');
  }

  dragAndDrop(event) {
    event.preventDefault();
    this.filesSelected(event.dataTransfer.items);
  }

  filesSelected(files) {
    if (
      files.length > this.maxImageNumber ||
      (files.length + this.imageNumber) > this.maxImageNumber
    ) {
      throw new Error();
    }

    for (let file of files) {

      if (
        (file.kind !== 'file' && !(file instanceof File)) ||
        this.allowedImageTypes.indexOf(file.type) === -1
      ) {
        throw new Error();
      }

      try {
        file = file.getAsFile();
      } catch (error) {
        file = file;
      }

      if (
        ((file.size / 1024) / 1024) > this.maxImageSize ||
        (this.postSize + ((file.size / 1024) / 1024)) > this.maxPostSize
      ) {
        throw new Error();
      }

      const fileReader = new FileReader();

      fileReader.addEventListener('load', () => {
        const imageContainer = document.createElement('div');
        imageContainer.setAttribute('class', 'uploaded-image-container');

        const imagePadding = document.createElement('div');
        imagePadding.setAttribute('class', 'uploaded-image-padding');

        const image = document.createElement('img');
        image.src = fileReader.result;
        image.alt = 'Uploaded image';
        image.setAttribute('class', 'uploaded-image');

        const imageInput = document.createElement('input');
        imageInput.type = 'hidden';
        imageInput.name = (
          this.imageInput.charAt(this.imageInput.length - 2) + this.imageInput.charAt(this.imageInput.length - 1) !== '[]'
        ) ?
        this.imageInput + '[]' :
        this.imageInput;
        imageInput.value = fileReader.result;

        const removeImageButton = document.createElement('button');
        removeImageButton.setAttribute('class', 'remove-image-button remove-image-button-hidden');
        removeImageButton.setAttribute('type', 'button');
        removeImageButton.innerHTML = 'Click to remove';

        imageContainer.addEventListener('click', (event: MouseEvent) => {
          this.addRemoveImageButton(event, removeImageButton);
        });

        removeImageButton.addEventListener('click', (event: MouseEvent) => {
          this.removeImage(event, file, imageContainer, imagePadding);
        });

        document.addEventListener('click', (event: MouseEvent) => {
          this.removeRemoveImageButton(event, removeImageButton);
          removeImageButton.setAttribute('class', 'remove-image-button remove-image-button-hidden');
        });

        imageContainer.appendChild(image);
        imageContainer.appendChild(imageInput);
        imageContainer.appendChild(removeImageButton);
        this.imageWrapper.appendChild(imageContainer);
        this.imageWrapper.appendChild(imagePadding);
      });

      fileReader.readAsDataURL(file);
      this.imageNumber += 1;
      this.postSize += ((file.size / 1024) / 1024);
    }
  }

  imageDialogChange(event) {
    this.filesSelected(event.target.files);
    event.target.value = '';
  }

  removeImage(event: MouseEvent, file: File, imageContainer: HTMLDivElement, imagePadding: HTMLDivElement) {
    event.stopPropagation();
    imageContainer.remove();
    imagePadding.remove();
    this.imageNumber -= 1;
    this.postSize -= ((file.size / 1024) / 1024);
  }

  removeRemoveImageButton(event: MouseEvent, removeImageButton: HTMLButtonElement) {
    event.stopPropagation();
    removeImageButton.setAttribute('class', 'remove-image-button remove-image-button-hidden');
  }

  uploadBtnClicked(event) {
    event.target.parentNode.children[1].click();
  }

  /**
   * Render Method
   */
  render() {
    return (
      <div id="imgn-upldr">
        <div
          class="upload-container"
          onDragOver={event => event.preventDefault()}
          onDrop={event => this.dragAndDrop(event)}>
          <h1>Drag & Drop Files or Click to Upload.</h1>
          {this.multiple
            ? <input
                id="file-dialog"
                type="file" onChange={event => this.imageDialogChange(event)}
                accept="image"
                multiple />
            : <input
              id="file-dialog"
              type="file"
              onChange={event => this.imageDialogChange(event)}
              accept="image" />
          }
          <button
            class="upload-button"
            onClick={event => this.uploadBtnClicked(event)}>
              Upload
          </button>
        </div>
        <br />
        <div
          class="image-container"
          ref={element => this.imageWrapper = element as HTMLDivElement}>
        </div>
      </div>
    );
  }
}
